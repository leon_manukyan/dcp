#!/usr/bin/env bash

mydir="$(dirname "$0")"
tmpdir=$(mktemp -d)
cleanup()
{
    for n in ${nodes[@]}
    do
        docker stop $n
    done
    docker network rm dcp_testing
    docker rmi dcp_testing
    rm -rf "$tmpdir"
}

trap cleanup EXIT

cp "$mydir"/../dcp "$tmpdir"
create_docker_network()
{
    docker network create dcp_testing &> /dev/null
    docker inspect -f '{{range .IPAM.Config}}{{.Subnet}}{{end}}' dcp_testing
}

subnet="$(create_docker_network)"
prefix_length="${subnet##*/}"
prefix_bytes=$((prefix_length/8))
subnetip="${subnet%%/*}"
subnetipbytes=(${subnetip//./ })
subnet_wildcard="${subnetipbytes[0]}"
for (( i=1; i<prefix_bytes; ++i ))
do
    subnet_wildcard+=".${subnetipbytes[i]}"
done
subnet_wildcard+=".*"

mkdir "$tmpdir"/.ssh
ssh-keygen -t rsa -N '' -f "$tmpdir"/.ssh/id_rsa
cp "$tmpdir"/.ssh/id_rsa.pub "$tmpdir"/.ssh/authorized_keys
cat > "$tmpdir"/.ssh/config <<END
Host $subnet_wildcard dcp_testing_node*
  IdentityFile ~/.ssh/id_rsa
  PreferredAuthentications publickey
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
END
chmod 600 "$tmpdir"/.ssh/config

cat > "$tmpdir"/Dockerfile <<'END'
FROM gotechnies/alpine-ssh

ADD .ssh /root/.ssh
ADD dcp /usr/bin/dcp
END
docker rmi dcp_testing
docker build -t dcp_testing "$tmpdir"
for i in {1..3}
do
    docker run --rm -d --name dcp_testing_node$i --network dcp_testing dcp_testing
    nodes+=(dcp_testing_node$i)
done
node1ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dcp_testing_node1)
ssh -o 'IdentitiesOnly=yes'                 \
    -o 'UserKnownHostsFile /dev/null'       \
    -o 'StrictHostKeyChecking no'           \
    -i "$tmpdir"/.ssh/id_rsa root@$node1ip  \
        dcp -m dcp_testing_node2,dcp_testing_node3 /usr/bin/dcp /root
